# Lab 8 - Exploratory testing and UI

## Homework

## Website
https://www.baeldung.com

As a homework you need to pick any website/application you want. Mention the website/app in your work, and check that works submitted(even without ready for review label) before you; your choice should be unique, one student - one application. After you've chosen your application to test, develop at least three exploratory test cases with a complete description of what you've tested. Then you will need to fully automate at least one of the tests using Selenium and push your code to GitLab together with the link to your Exploratotry tests (or the tests themselves).

### Extra
I will put one more point if your Selenium test automation would be in GitLab CI and there would not be a driver binary in the repository.
