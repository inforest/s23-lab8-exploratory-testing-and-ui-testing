from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# Create an instance of the Chrome driver
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
driver = webdriver.Chrome(options=options)

# Load the page
driver.get("https://www.baeldung.com")

# Wait 3 seconds
time.sleep(3)

# Wait for an element in the footer of the page and follow the link
privacy_policy_link = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT, "PRIVACY POLICY")))
privacy_policy_link.click()

# Checking the title of the privacy policy page
assert "Privacy Policy" in driver.title

# Close browser
driver.quit()

